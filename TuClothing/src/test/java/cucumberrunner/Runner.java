package cucumberrunner;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue="stepDefination",
        features = "src/test/resources/features")


public class Runner {

}
