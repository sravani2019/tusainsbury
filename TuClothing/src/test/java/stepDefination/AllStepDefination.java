package stepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllStepDefination extends AbstractMain{

	@Given("^I am in home page$")
	public void i_am_in_home_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
	    driver=new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");
		
	}

	@When("^I enter valid product name$")
	public void i_enter_valid_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#search")).clear();
		driver.findElement(By.cssSelector("#search")).sendKeys("jeans");
		driver.findElement(By.cssSelector("button[alt='Search']")).click();
      
	}

	@Then("^I should see search product result page$")
	public void i_should_see_search_product_result_page() throws Throwable {
		  driver.close();  
	}



}
